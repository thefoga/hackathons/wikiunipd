# WikiUnipd

Wanna see the best library near you? The least crowded studyroom nearby your eating place?
This app is for ya.

##Purpose
While hacking in [H-ACK Unipd](http://h-ack.com/unipd) the Pop-up team come up with this very interesting solution that made them win the Contest #1.

##Ok, ok I understood you're awesome, but the app? Is it?
Yes, sure it is. There is also a [presentation](presentation.pdf) (and the [Prezi](https://prezi.com/alohugttu7nb) counterpart) set up by the Biz team that helps you see all the app advantages.

## How about the setup?

* Ask [prof18](https://github.com/prof18) or [sirfoga](https://github.com/sirfoga) for help (in exchange of RedBulls)
* Configure it to your needs
* Remember: #inRedBullWeTrust

