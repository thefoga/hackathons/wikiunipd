# Change Log
All notable changes to this project will be documented in this file.

## [version] - {yyyy}-{mm}-{dd}

### Added
- something has been added

### Changed
- something has changed

### Fixed
- somebug has been fixed

### Removed
- because it was too old

## [a previous version] - {yyyy}-{mm}-{dd}

### Added
- something has been added

### Changed
- something has changed

### Fixed
- somebug has been fixed

### Removed
- because it was too old
